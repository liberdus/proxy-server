# Proxy Server for Liberdus Clients
## Introduction
The main purpose of proxy server is to proxy `https` requests from web and mobile clients to consensor nodes (which 
support `http` connection only). 

## Prerequisites
Proxy server can be deployed on a cloud server which has `docker` and `docker-compose` packages installed. Detail 
requirements are presented below:
- A cloud server instance
- A domain name must be registered and point to the cloud server IP address
- The server must have `docker` and `docker-compose` packages

## Setup
- Make sure desired domain name is pointed to server's IP address
- Clone this repository on the cloud server
- Make sure `init-letsencrypt.sh` is executable
  - run `chmod +x init-letsencrypt.sh`
- Run `init-letsencrypt.sh` with desired domain name and email address
  - `sudo ./init-letsencrypt.sh proxy1.liberdus.com johndoe@gmail.com`

## How it works
- Nginx is used to proxy the requests from the clients and Certbot is used to generate SSL certificates. 
- Nginx and Certbot docker images work together to complete `acme` challenge from LetsEncrypt. 
- However, to complete the `acme`challenge, SSL certificates are required to start Nginx server (an example of chicken and egg origin problem).
- Therefore, a custom bash script `init-letsencrypt.sh` is used to generate dummy certificates first and complete the 
`acme` challenges.
- After the challenge is completed, it deletes the dummy certificates and reloads the Nginx 
container with genuine certificates.
